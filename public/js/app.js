'use strict';

var UsersRootApp = new Marionette.Application();

UsersRootApp.addRegions({
    navigationRegion: '#navigation-region',
    mainRegion: '#main-region',
    dialogRegion: Marionette.Region.Dialog.extend({
        el: '#dialog-region'
    })
});

UsersRootApp.navigate = function(route, options) {
    options || (options = {});
    Backbone.history.navigate(route, options);
};

UsersRootApp.getCurrentRoute = function() {
    return Backbone.history.fragment
};

UsersRootApp.on('start', function() {
    if (Backbone.history) {
        Backbone.history.start();

        if (this.getCurrentRoute() === '') {
            UsersRootApp.trigger('contacts:list');
        }
    }
});