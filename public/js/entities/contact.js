'use strict';

UsersRootApp.module('Entities', function(Entities, ContactManager, Backbone, Marionette, $, _) {
    Entities.Contact = Backbone.Model.extend({

        urlRoot: apiUrl,

        url: function() {

            // Customize requests
            if (this.isNew()) {
                return this.urlRoot + '/add';
            } else {
                return this.urlRoot + '/' + this.id;
            }

        },

        defaults: {
            name: '',
            email: '',
            role: ''
        },

        validate: function(attrs, options) {
            var errors = {};

            if (!attrs.name) {
                errors.name = 'polje ne sme biti prazno';
            }
            if (!attrs.email) {
                errors.email = 'polje ne sme biti prazno';
            }
            if (!attrs.role) {
                errors.role = 'polje ne sme biti prazno';
            }
            if (!_.isEmpty(errors)) {
                return errors;
            }
        }
    });


    Entities.ContactCollection = Backbone.Collection.extend({
        url: apiUrl,
        model: Entities.Contact,
        comparator: 'name'
    });

    var API = {
        getContactEntities: function() {

            var contacts = new Entities.ContactCollection();
            var defer = $.Deferred();

            // Modify retrived data
            contacts.parse = function(response) {
                return _.toArray(response);
            };

            contacts.fetch({
                success: function(data) {
                    defer.resolve(data);
                }
            });

            return defer.promise();

        },

        getContactEntity: function(contactId) {

            var contact = new Entities.Contact({
                id: contactId
            });
            var defer = $.Deferred();

            contact.fetch({
                success: function(data) {
                    defer.resolve(data);
                },
                error: function(data) {
                    defer.resolve(undefined);
                }
            });

            return defer.promise();

        }
    };

    UsersRootApp.reqres.setHandler('contact:entities', function() {
        return API.getContactEntities();
    });

    UsersRootApp.reqres.setHandler('contact:entity', function(id) {
        return API.getContactEntity(id);
    });
});