UsersRootApp.module("NavigationApp", function(Navigation, ContactManager, Backbone, Marionette, $, _){
  var API = {
    listNavigation: function(){
      Navigation.List.Controller.listNavigation();
    }
  };

  UsersRootApp.commands.setHandler("set:active:navigation", function(name){
    UsersRootApp.NavigationApp.List.Controller.setActiveNavigation(name);
  });

  Navigation.on("start", function(){
    API.listNavigation();
  });
});
