UsersRootApp.module("NavigationApp.List", function(List, ContactManager, Backbone, Marionette, $, _){
  List.Controller = {
    listNavigation: function(){
      var links = UsersRootApp.request("navigation:entities");
      var navigations = new List.Headers({collection: links});

      navigations.on("brand:clicked", function(){
        UsersRootApp.trigger("contacts:list");
      });

      navigations.on("childview:navigate", function(childView, model){
        var trigger = model.get("navigationTrigger");
        UsersRootApp.trigger(trigger);
      });

      UsersRootApp.navigationRegion.show(navigations);
    },

    setActiveNavigation: function(navigationUrl){
      var links = UsersRootApp.request("navigation:entities");
      var navigationToSelect = links.find(function(navigation){ return navigation.get("url") === navigationUrl; });
      navigationToSelect.select();
      links.trigger("reset");
    }
  };
});
