'use strict';

UsersRootApp.module('ContactsApp', function(ContactsApp, ContactManager, Backbone, Marionette, $, _) {
    ContactsApp.Router = Marionette.AppRouter.extend({
        appRoutes: {
            'users(/)': 'listContacts',
            'users/:id': 'showContact',
            'users/:id/edit': 'editContact'
        }
    });

    var API = {
        listContacts: function() {
            ContactsApp.List.Controller.listContacts();
            UsersRootApp.execute('set:active:navigation', 'users');
        },

        showContact: function(id) {
            ContactsApp.Show.Controller.showContact(id);
            UsersRootApp.execute('set:active:navigation', 'users');
        },

        editContact: function(id) {
            ContactsApp.Edit.Controller.editContact(id);
            UsersRootApp.execute('set:active:navigation', 'users');
        }
    };

    UsersRootApp.on('contacts:list', function() {
        UsersRootApp.navigate('users');
        API.listContacts();
    });

    UsersRootApp.on('contact:show', function(id) {
        UsersRootApp.navigate('users/' + id);
        API.showContact(id);
    });

    UsersRootApp.on('contact:edit', function(id) {
        UsersRootApp.navigate('users/' + id + '/edit');
        API.editContact(id);
    });

    UsersRootApp.addInitializer(function() {
        new ContactsApp.Router({
            controller: API
        });
    });
});