'use strict';

UsersRootApp.module('ContactsApp.List', function(List, ContactManager, Backbone, Marionette, $, _) {
    List.Controller = {
        listContacts: function() {
            var loadingView = new UsersRootApp.Common.Views.Loading();
            UsersRootApp.mainRegion.show(loadingView);

            var fetchingContacts = UsersRootApp.request('contact:entities');

            var contactsListLayout = new List.Layout();
            var contactsListPanel = new List.Panel();

            $.when(fetchingContacts).done(function(contacts) {

                var contactsListView = new List.Contacts({
                    collection: contacts
                });

                contactsListLayout.on('show', function() {
                    contactsListLayout.panelRegion.show(contactsListPanel);
                    contactsListLayout.contactsRegion.show(contactsListView);
                });

                contactsListPanel.on('contact:new', function() {
                    var newContact = new UsersRootApp.Entities.Contact();

                    var view = new UsersRootApp.ContactsApp.New.Contact({
                        model: newContact
                    });

                    view.on('form:submit', function(data) {

                        if (newContact.save(data)) {
                            contacts.add(newContact);
                            view.trigger('dialog:close');
                            var newContactView = contactsListView.children.findByModel(newContact);

                            if (newContactView) {
                                newContactView.flash('Uspešno');
                            }
                        } else {
                            view.triggerMethod('form:data:invalid', newContact.validationError);
                        }
                    });

                    UsersRootApp.dialogRegion.show(view);
                });

                contactsListView.on('childview:contact:show', function(childView, args) {
                    UsersRootApp.trigger('contact:show', args.model.get('id'));
                });

                contactsListView.on('childview:contact:edit', function(childView, args) {
                    var model = args.model;
                    var view = new UsersRootApp.ContactsApp.Edit.Contact({
                        model: model
                    });

                    view.on('form:submit', function(data) {
                        if (model.save(data)) {
                            childView.render();
                            view.trigger('dialog:close');
                            childView.flash('success');
                        } else {
                            view.triggerMethod('form:data:invalid', model.validationError);
                        }
                    });

                    UsersRootApp.dialogRegion.show(view);
                });

                contactsListView.on('childview:contact:delete', function(childView, args) {
                    args.model.destroy();
                });

                UsersRootApp.mainRegion.show(contactsListLayout);
            });
        }
    };
});