'use strict';

UsersRootApp.module('ContactsApp.Show', function(Show, ContactManager, Backbone, Marionette, $, _) {
    Show.Controller = {
        showContact: function(id) {

            var fetchingContact = UsersRootApp.request('contact:entity', id);
            $.when(fetchingContact).done(function(contact) {
                var contactView;
                if (contact !== undefined) {
                    contactView = new Show.Contact({
                        model: contact
                    });

                    contactView.on('contact:edit', function(contact) {
                        UsersRootApp.trigger('contact:edit', contact.get('id'));
                    });
                } else {
                    contactView = new Show.MissingContact();
                }

                UsersRootApp.mainRegion.show(contactView);
            });
        }
    }
});