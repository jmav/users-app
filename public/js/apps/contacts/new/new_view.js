'use strict';

UsersRootApp.module('ContactsApp.New', function(New, ContactManager, Backbone, Marionette, $, _) {
    New.Contact = UsersRootApp.ContactsApp.Common.Views.Form.extend({
        title: 'Nov uporabnik',

        onRender: function() {
            this.$('.js-submit').text('Ustvari uporabnika');
        }
    });
});