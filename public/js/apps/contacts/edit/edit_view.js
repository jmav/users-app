'use strict';

UsersRootApp.module('ContactsApp.Edit', function(Edit, ContactManager, Backbone, Marionette, $, _) {
    Edit.Contact = UsersRootApp.ContactsApp.Common.Views.Form.extend({
        initialize: function() {
            this.title = 'Uredi ' + this.model.get('name');
        },

        onRender: function() {
            if (this.options.generateTitle) {
                var $title = $('<h1>', {
                    text: this.title
                });
                this.$el.prepend($title);
            }

            this.$('.js-submit').text('Posodobi uporabnika');
        }
    });
});