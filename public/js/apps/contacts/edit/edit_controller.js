'use strict';

UsersRootApp.module('ContactsApp.Edit', function(Edit, ContactManager, Backbone, Marionette, $, _) {
    Edit.Controller = {
        editContact: function(id) {

            var fetchingContact = UsersRootApp.request('contact:entity', id);
            $.when(fetchingContact).done(function(contact) {
                var view;
                if (contact !== undefined) {
                    view = new Edit.Contact({
                        model: contact,
                        generateTitle: true
                    });

                    view.on('form:submit', function(data) {
                        if (contact.save(data)) {
                            UsersRootApp.trigger('contact:show', contact.get('id'));
                        } else {
                            view.triggerMethod('form:data:invalid', contact.validationError);
                        }
                    });
                } else {
                    view = new UsersRootApp.ContactsApp.Show.MissingContact();
                }

                UsersRootApp.mainRegion.show(view);
            });
        }
    };
});