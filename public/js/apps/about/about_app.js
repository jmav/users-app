'use strict';

UsersRootApp.module('AboutApp', function(AboutApp, ContactManager, Backbone, Marionette, $, _) {
    AboutApp.Router = Marionette.AppRouter.extend({
        appRoutes: {
            'about': 'showAbout'
        }
    });

    var API = {
        showAbout: function() {
            AboutApp.Show.Controller.showAbout();
            UsersRootApp.execute('set:active:navigation', 'about');
        }
    };

    UsersRootApp.on('about:show', function() {
        UsersRootApp.navigate('about');
        API.showAbout();
    });

    UsersRootApp.addInitializer(function() {
        new AboutApp.Router({
            controller: API
        });
    });
});