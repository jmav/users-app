
Demo users application
---


Base concept how to integrate Backbone & Marionette with API. 

Created | Mar. 2015
--------|-------
Author  | Jure Mav


## Getting Started


### Used tools

- Sublime text
- Google chrome

### Prerequisites apps

Build process relies on following essential tools/apps:

- [node.js](http://nodejs.org/)
- [npm](http://nodejs.org/)

Browser part of app relies on components defined in bwer.json ([Bower](http://bower.io/)).


### Initial setup

1. Clone repository: `git@bitbucket.org:jmav/users-app.git`
2. Goto root of directory.
3. Install Prerequisites apps as globals
4. Run in terminal @root of directory:
	- `npm install` to install additional command line tools
	- `bower install` to install browser dependencies
		(Use backbone 1.0.xx)
5. Start node server `node server/app.js`
6. Go to browser `http://localhost:3000` (port is configurable in `config.json` file)
7. You should get app index page


**Users app demo, video link.**

[![Users app demo](http://img.youtube.com/vi/iv6tGq4MndQ/0.jpg)](http://www.youtube.com/watch?v=iv6tGq4MndQ)


## Local API server

API server is based on [sails.js](http://sailsjs.org/) framework. To start the server go to `api` directory & run:

	sails lift

Than configure `/config.js` apiUrl parameter with

	'http://localhost:1337/user'

At the end run local app server `node server/app.js` & try the application.

## Todo

- Grunt optimization for production
- Precompile HBS templates
- Implementation API messages - current response format is not by standard, client rules needs rewrites
- Add less support



## Known API bugs

**Ovira:** Access-Control-Allow-Origin  
Lokalno sem pripravil proxy strežnik, ki prepošilja api klice na testni API strežnik.

**Ovira:** POST/PUT ne sprejema json formata  
Post zahteva podatke kot "form-data", ne sprejme json-a. Sem poskušal z modifikacijo, vendar je premalo časa. Vključen mora biti tudi cookie.

**Ovira:** DELETE 411 (Length Required)  
Težava je v proxiyu
