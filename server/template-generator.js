/**
 * Template generator
 */

'use strict';

var grunt = require('grunt'),
    path = require('path'),
    cons = require('consolidate');

exports.index = function(success, enviroment) {

    var config = grunt.file.readJSON('config.json'),
        cssList = [],
        jsList = [],
        templates = [];

    // Defaults
    enviroment = enviroment || config.enviroment;

    if (enviroment === 'dev') {

        cssList = [
            'css/bootstrap.css',
            'css/application.css',
            'lib/jqueryui/themes/base/jquery-ui.min.css'
        ];

        jsList = [
            'lib/jquery/dist/jquery.min.js',
            'lib/jqueryui/jquery-ui.min.js',
            'lib/json2/json2.js',
            'lib/underscore/underscore.js',
            'lib/backbone/backbone.js',

            'lib/backbone.picky/lib/backbone.picky.min.js',
            'lib/backbone.syphon/lib/backbone.syphon.min.js',
            'lib/backbone.marionette/lib/backbone.marionette.min.js',
            'lib/handlebars/handlebars.js',
            'lib/marionette.handlebars/dist/marionette.handlebars.min.js',

            'js/legacy/spin.js',
            'js/legacy/spin.jquery.js',


            'js/apps/config/marionette/regions/dialog.js',
            'js/app.js',
            'js/entities/navigation.js',
            'js/entities/contact.js',
            'js/common/views.js',

            'js/apps/contacts/contacts_app.js',
            'js/apps/contacts/common/views.js',
            'js/apps/contacts/list/list_view.js',
            'js/apps/contacts/list/list_controller.js',
            'js/apps/contacts/show/show_view.js',
            'js/apps/contacts/show/show_controller.js',
            'js/apps/contacts/edit/edit_view.js',
            'js/apps/contacts/edit/edit_controller.js',
            'js/apps/contacts/new/new_view.js',

            'js/apps/about/about_app.js',
            'js/apps/about/show/show_view.js',
            'js/apps/about/show/show_controller.js',

            'js/apps/navigation/navigation_app.js',
            'js/apps/navigation/list/list_view.js',
            'js/apps/navigation/list/list_controller.js'
        ];

        grunt.file.expand(config.staticSrc.templates + '/*' + config.templateExt).map(function(filePath) {
            var template = {};
            template.name = path.basename(path.relative(config.staticSrc.templates, filePath), config.templateExt);
            template.content = grunt.file.read(filePath);
            templates.push(template);
        });

    }

    cons.handlebars('server/templates/index.hbs', {
        jsList: jsList,
        cssList: cssList,
        templates: templates,
        apiUrl: config.apiUrl
    }, function(err, html) {

        if (err) {
            throw err;
        }
        success(html);

    });
};