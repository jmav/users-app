/**
 * Http server app
 */

'use strict';

var express = require('express'),
    app = express(),
    // app = require('express')(),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    grunt = require('grunt'),
    indexGenerator = require('./template-generator.js'),
    request = require('request');

require('request-debug')(request, function(type, data, r) {
    if (type === 'request') {

        console.log('req data ---->', data);
        // console.log('headers ----->', r.headers);
    }
    // put your request or response handling logic here
});


app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({
    extended: true
})); // for parsing application/x-www-form-urlencoded
app.use(multer()); // for parsing multipart/form-data

var config = grunt.file.readJSON('config.json'),
    apiUrl = 'http://test-app-api.dev.dlabs.si';

console.log('Enviroment: ' + config.enviroment);

if (config.enviroment === 'dev') {
    app.use('/public', express.static('public'));
}


// Proxy
// Missing -> No 'Access-Control-Allow-Origin'
app.use('/api', function(req, res) {

    var url = apiUrl + req.url,
        r = null,
        headers = {
            Accept: '*/*',
            Cookie: 'PHPSESSID=i2cra9s1ucm5980qau64n3djo5',
        };



    // when it's post request set up a request.post to have our post body
    if (req.method === 'POST' || req.method === 'PUT') {

        var method = req.method.toLowerCase();

        r = request[method]({
            uri: url,
            json: req.body,
            headers: headers

        });

    } else if (req.method === 'DELETE') {

        r = request.del({
            url: url,
            headers: headers
        });

    } else {

        r = request({
            url: url,
            headers: headers
        });

    }

    console.log('Proxy (%s): %s', req.method, url);

    req.pipe(r).pipe(res);

});


// INDEX
app.get('/', function(req, res) {
    indexGenerator.index(function(html) {
        res.send(html);
    });
});


var server = app.listen(config.port, function() {
    console.log('Listening on port %d', server.address().port);
});